﻿using UnityEngine;
using UnityEngine.UI;

public class PLanetScreenManager : MonoBehaviour
{
    public Text NameElement;
    public Text CapElement;
    public Text PopElement;
    public Text GrowElement;
    public Planet planet;
    public Text FoodElement;
    public Text FoodGenElement;
    public Text ResourcesElement;


    public Text RunVal;
    public Text HabVal;

    public Text HapVal;
    public Text HapwillVal;
    public Text HapwithVal;

    public Text MineVal;
    public Text FarmVal;
    public Text ResourcesGenElement;
    public Text FoodProdVal;

    public GameObject[] slots;


    public GameObject RBunk;
    public GameObject RPart;
    public GameObject RFlub;
    public GameObject RCry;
    public GameObject RDown;

    private string currentTab = "Main";
    public Canvas BuildCanvas;
    public Canvas ShipCanvas;
    public Canvas ProductionCanvas;
    public Canvas PopulationCanvas;

    public Sprite mine;

    public void SetPlanet(Planet planet)
    {
        this.planet = planet;
        currentTab = "Main";
        SwitchTab("Main");
        UpdateUI();
    }

    public void SetPlanetName(string name)
    {
        NameElement.text = name;
    }

    public void SetGrowth(string grow)
    {
        GrowElement.text = grow;
    }

    public void SetPop(string pop)
    {
        PopElement.text = pop;
    }

    public void SetCap(string cap)
    {
        CapElement.text = cap;
    }

    public void SwitchTab(string tab)
    {
        currentTab = tab;
        if (tab == "Build")
        {
            BuildCanvas.enabled = true;
            ProductionCanvas.enabled = false;
            ShipCanvas.enabled = false;
        }
        else if (tab == "Main")
        {
            BuildCanvas.enabled = false;
            ProductionCanvas.enabled = true;
            ShipCanvas.enabled = false;
        }
        else if (tab == "Research")
        {
            BuildCanvas.enabled = false;
            ProductionCanvas.enabled = false;
            ShipCanvas.enabled = true;
        }

        UpdateBuildQeueu();
    }

    public void Build(Button sevent)
    {
        if (sevent.name == "Mine")
        {
            if (this.planet.Resources >= 200f)
            {
                var sprite = Resources.Load<Sprite>("miningpng");
                var job = new BuildMineJob(6, sprite);
                this.planet.Resources -= 200;
                planet.AddJob(job);
                UpdateBuildQeueu();
            }
        }

        if (sevent.name == "Colony")
        {
            if (this.planet.Resources >= 400 && this.planet.Food >= 50)
            {
                this.planet.Resources -= 400;
                this.planet.Food -= 50;
                var sprite = Resources.Load<Sprite>("colony");
                var job = new BuildColonyShipJob(8, sprite);
                planet.AddJob(job);
                UpdateBuildQeueu();
            }
        }


        if (sevent.name == "Farm")
        {
            if (this.planet.Resources >= 500)
            {
                this.planet.Resources -= 500;
                var sprite = Resources.Load<Sprite>("Farm");
                var job = new BuildFarmJob(4, sprite);
                planet.AddJob(job);

                UpdateBuildQeueu();
            }
        }

        UpdateUI();
    }

    public void setBuild(string type)
    {
        switch (type)
        {
            case "Bunk":
                this.planet.HasBuildBunk = true;
                break;
            case "Cry":
                this.planet.HasBuildCry = true;
                break;
            case "Flub":
                this.planet.HasBuildFlub = true;
                break;
            case "Down":
                this.planet.HasBuildDown = true;
                break;
            case "Part":
                this.planet.HasBuildPart = true;
                break;
        }

        UpdateUI();
    }

    public void Research(int type)
    {
        switch (type)
        {
            case 1:
                if (this.planet.Resources >= 250f)
                {
                    var sprite = Resources.Load<Sprite>("cryo");
                    var job = new ResearchCryo(6, sprite);
                    this.planet.Resources -= 250f;
                    planet.AddResearch(job);
                    UpdateBuildQeueu();
                }

                break;
            case 2:
                if (this.planet.Resources >= 400f)
                {
                    var sprite = Resources.Load<Sprite>("down");
                    var job = new ResearchDown(8, sprite);
                    this.planet.Resources -= 400f;
                    planet.AddResearch(job);
                    UpdateBuildQeueu();
                }

                break;
            case 3:
                if (this.planet.Food >= 30f)
                {
                    var sprite = Resources.Load<Sprite>("flub");
                    var job = new ResearchFlub(2, sprite);
                    this.planet.Food -= 30f;
                    planet.AddResearch(job);
                    UpdateBuildQeueu();
                }

                break;

            case 4:
                if (this.planet.Resources >= 50f)
                {
                    var sprite = Resources.Load<Sprite>("bunk");
                    var job = new ResearchCryo(2, sprite);
                    this.planet.Resources -= 50f;
                    planet.AddResearch(job);
                    UpdateBuildQeueu();
                }

                break;
            case 5:
                if (this.planet.Resources >= 500)
                {
                    var sprite = Resources.Load<Sprite>("part");
                    var job = new ResearchPart(9, sprite);
                    this.planet.Resources -= 500f;
                    planet.AddResearch(job);
                    UpdateBuildQeueu();
                }

                break;
            case 6:
            {
                var sprite = Resources.Load<Sprite>("soy");
                var job = new ResearchSoylent(3, sprite);
                planet.AddResearch(job);
                UpdateBuildQeueu();

                break;
            }
            case 7:
            {
                if (this.planet.Resources >= 3 && this.planet.Food >= 1f)
                {
                    var sprite = Resources.Load<Sprite>("fur");
                    var job = new ResearchFurr(1, sprite);
                    planet.AddResearch(job);
                    this.planet.Resources -= 3f;
                    this.planet.Food -= 1f;

                    UpdateBuildQeueu();
                }

                break;
            }
        }
    }

    public void UpdateBuildQeueu()
    {
        var i = 0;
        for (var x = 0; x < 4; x++)
        {
            slots[x].GetComponentInChildren<Image>().sprite = null;
            slots[x].GetComponentInChildren<Image>().enabled = false;
            slots[x].GetComponentInChildren<Text>().text = "";
        }

        if (currentTab == "Research")
        {
            foreach (var planetJob in planet.Research)
            {
                if (i < slots.Length)
                {
                    slots[i].GetComponentInChildren<Image>().sprite = planetJob.JobSprite;
                    slots[i].GetComponentInChildren<Text>().text = planetJob.TurnsLeft.ToString() + " Months";
                    slots[i].GetComponentInChildren<Image>().enabled = true;
                    i++;
                }
            }
        }
        else
        {
            foreach (var planetJob in planet.Jobs)
            {
                if (i < slots.Length)
                {
                    slots[i].GetComponentInChildren<Image>().sprite = planetJob.JobSprite;
                    slots[i].GetComponentInChildren<Text>().text = planetJob.TurnsLeft.ToString() + " Months";
                    slots[i].GetComponentInChildren<Image>().enabled = true;
                    i++;
                }
            }
        }
    }

    public void UpdateUI()
    {
        SetPlanetName(planet.Name);
        SetGrowth(planet.PopulationModifier.ToString());
        SetPop(planet.CurrentPopulation.ToString());
        SetCap(planet.MaxPopulation.ToString());


        UpdateBuildQeueu();
        SetResearchButtons();

        FoodElement.text = planet.Food.ToString();
        ResourcesElement.text = planet.Resources.ToString();
//        ResourcesGenElement.text = planet.ResourcesGen.ToString();


        HabVal.text = planet.PopulationModifier.ToString();
        HapVal.text = planet.Happiness.ToString();
        HapwillVal.text = (planet.CurrentPopulation > planet.MaxPopulation) ? "Fall" : "Rise";
        HapwithVal.text = (planet.CurrentPopulation > planet.MaxPopulation)
            ? planet.GetLesserHappiness().ToString()
            : planet.ExtraHappiness().ToString();

        MineVal.text = planet.Mines.ToString();
        FarmVal.text = planet.Farms.ToString();
        ResourcesElement.text = (planet.Mines * planet.ResourcesGen).ToString();
        FoodGenElement.text = planet.GetFoodGen().ToString();
        GrowElement.text = ((planet.CurrentPopulation / 100) * planet.PopulationModifier).ToString();
    }

    private void SetResearchButtons()
    {
        if (planet.HasBuildBunk)
        {
            RBunk.SetActive(false);
        }
        else
        {
            RBunk.SetActive(true);
        }

        if (planet.HasBuildCry)
        {
            RCry.SetActive(false);    
        }
        else
        {
            RCry.SetActive(true);
        }

        if (planet.HasBuildPart)
        {
            RPart.SetActive(false);
        }
        else
        {
            RPart.SetActive(true);
        }

        if (planet.HasBuildDown)
        {
            RDown.SetActive(false);
        }
        else
        {
            RDown.SetActive(true);
        }

        if (planet.HasBuildFlub)
        {
            RFlub.SetActive(false);
        }
        else
        {
            RFlub.SetActive(true);
        }
    }

    public void Close()
    {
        this.gameObject.SetActive(false);
    }
}