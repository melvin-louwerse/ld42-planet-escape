﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Universe : MonoBehaviour
{
    public Planet[] planets;
    public PLanetScreenManager UiScreen;

    private float _fireStartTime;

    public float populationIncreaseTime = 60f;

    private int _turn = 1;
    public HashSet<Ship> ships;

    public Text turnI;
    public Text part;
    public GameObject ActivePlanet;

    public Planet CurrentPlanet;

    // Use this for initialization
    void Start()
    {
        turnI.text = 1.ToString();
        ships = new HashSet<Ship>();
        _fireStartTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (ActivePlanet != null)
        {
            Vector3 currentPos = ActivePlanet.transform.position;
            currentPos.z = -10;

            Camera.main.transform.position = currentPos;
        }
    }


    public void AddShip(Ship ship)
    {
        this.ships.Add(ship);
    }

    public void DoTurn()
    {
        turnI.text = (++_turn).ToString();
        var parts = 0;
        foreach (var planet in planets)
        {
            if (planet.Colonized)
            {
                planet.DoTurn();
                UiScreen.UpdateBuildQeueu();
                UiScreen.UpdateUI();

                if (planet.Happiness == 0)
                {
                    SceneManager.LoadScene("Gameover");
                }

                parts += planet.PartsDone;
            }
        }
        part.text = parts.ToString();

        if (parts >= 5)
        {
            SceneManager.LoadScene("Yay");
        }

        HashSet<Ship> deleable = new HashSet<Ship>();
        foreach (var ship in ships)
        {
            ship.DoTurn();

            if (ship.turnsFinished())
            {
                deleable.Add(ship);
            }

            UiScreen.UpdateUI();
        }

        foreach (var ship in deleable)
        {
            ships.Remove(ship);
        }
    }


    public void ExitGame()
    {
        Application.Quit();
    }
}