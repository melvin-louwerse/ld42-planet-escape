using UnityEngine;
using Random = System.Random;

public class ResearchFurr : Job
{
    public ResearchFurr(int turnsLeft, Sprite jobSprite) : base(turnsLeft, jobSprite)
    {
    }

    public override void DoJobAction(Planet planet)
    {
        planet.CurrentPopulation -= 100;
    }
}