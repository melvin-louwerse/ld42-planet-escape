using UnityEngine;

public class ResearchCryo : Job
{
    public ResearchCryo(int turnsLeft, Sprite jobSprite) : base(turnsLeft, jobSprite)
    {
    }

    public override void DoJobAction(Planet planet)
    {
        planet.CurrentPopulation = ((planet.CurrentPopulation / 100) * 66);
    }
}