using UnityEngine;

public class ResearchDown : Job
{
    public ResearchDown(int turnsLeft, Sprite jobSprite) : base(turnsLeft, jobSprite)
    {
    }

    public override void DoJobAction(Planet planet)
    {
        planet.CurrentPopulation = ((planet.CurrentPopulation / 100) * 90);
    }
}