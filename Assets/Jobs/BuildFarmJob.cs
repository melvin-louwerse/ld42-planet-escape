using UnityEngine;

public class BuildFarmJob : Job
{
    public BuildFarmJob(int turnsLeft, Sprite jobSprite) : base(turnsLeft, jobSprite)
    {
    }

    public override void DoJobAction(Planet planet)
    {
        planet.Farms ++;
    }
}