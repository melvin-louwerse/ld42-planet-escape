using UnityEngine;

public class ResearchBunk : Job
{
    public ResearchBunk(int turnsLeft, Sprite jobSprite) : base(turnsLeft, jobSprite)
    {
    }

    public override void DoJobAction(Planet planet)
    {
        planet.MaxPopulation += 300;
        planet.HasBuildBunk = true;
    }
}