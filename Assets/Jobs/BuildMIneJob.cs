using UnityEngine;

public class BuildMineJob : Job
{
    public BuildMineJob(int turnsLeft, Sprite jobSprite) : base(turnsLeft, jobSprite)
    {
    }

    public override void DoJobAction(Planet planet)
    {
        planet.Mines ++;
    }
}