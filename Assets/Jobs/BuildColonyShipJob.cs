using UnityEngine;

public class BuildColonyShipJob : Job
{
    public BuildColonyShipJob(int turnsLeft, Sprite jobSprite) : base(turnsLeft, jobSprite)
    {
    }

    public override void DoJobAction(Planet planet)
    {
        planet.AddShip("Colony");
    }
}