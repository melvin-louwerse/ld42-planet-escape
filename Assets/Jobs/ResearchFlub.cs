using UnityEngine;

public class ResearchFlub : Job
{
    public ResearchFlub(int turnsLeft, Sprite jobSprite) : base(turnsLeft, jobSprite)
    {
    }

    public override void DoJobAction(Planet planet)
    {
        planet.setHappiness(100);
    }
}