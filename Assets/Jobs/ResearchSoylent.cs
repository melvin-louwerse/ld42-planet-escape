using UnityEngine;
using Random = System.Random;

public class ResearchSoylent : Job
{
    private Random rnd;

    public ResearchSoylent(int turnsLeft, Sprite jobSprite) : base(turnsLeft, jobSprite)
    {
        rnd = new Random();
    }

    public override void DoJobAction(Planet planet)
    {
        planet.CurrentPopulation -= 1000;
        planet.Food += 100;
        int chance = rnd.Next(100);
        if (chance > 49)
        {
            planet.setHappiness(planet.Happiness - 25f);
        }
    }
}