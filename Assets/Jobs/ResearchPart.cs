using UnityEngine;

public class ResearchPart : Job
{
    public ResearchPart(int turnsLeft, Sprite jobSprite) : base(turnsLeft, jobSprite)
    {
    }

    public override void DoJobAction(Planet planet)
    {
        planet.PartsDone += 1;
    }
}