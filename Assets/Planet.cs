﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Planet : MonoBehaviour
{
    public int MaxPopulation;
    public int CurrentPopulation;
    public string Name;
    public float PopulationModifier;
    public float Happiness = 100f;

    public int Farms = 0;
    public float Food = 100;
    public float FoodGen = 25f;
    public float BaseFoodGen = 9f;


    public int Mines = 0;
    public float ResourcesGen = 50f;
    public float Resources = 100;

    public Text indicator;

    public Sprite Happy;
    public Sprite Meh;
    public Sprite Sad;
    public TextMesh PopLab;
    public TextMesh CapLab;
    public SpriteRenderer Smiley;
    public HashSet<Job> Jobs { get; private set; }
    public HashSet<Job> Research { get; private set; }
    public HashSet<Ship> Ships { get; private set; }
    public int PartsDone { get; set; }

    public GameObject parent;
    public Vector3 axis = new Vector3(0, 1, 1);

    public GameObject[] ShipSlots;

    public GameObject TextContainer;
    public bool Colonized = false;
    public float RotateSpeed = 0.5f;
    public float Radius = 59f;


    public bool HasBuildPart = false;
    public bool HasBuildDown = false;
    public bool HasBuildBunk = false;
    public bool HasBuildFlub = false;
    public bool HasBuildCry = false;

    private Vector2 _centre;
    private float _angle;


    public void setHappiness(float h)
    {
        this.Happiness = h;
    }

    public void DoTurn()
    {
        if (indicator != null)
        {
            indicator.enabled = false;
        }

        int increase = (int) ((CurrentPopulation / 100) * PopulationModifier);
        CurrentPopulation += increase;
        UpdateUi();
        UpdateJobs();
        UpdateResearch();
        UpdateFood();
        UpdateResource();
        UpdateHappiness();
    }

    private void UpdateHappiness()
    {
        if (CurrentPopulation > MaxPopulation)
        {
            var LHappiness = GetLesserHappiness();
            setHappiness(Mathf.Max(0, LHappiness));
        }
        else
        {
            var LHappiness = ExtraHappiness();
            setHappiness(Mathf.Min(100, LHappiness));
        }
    }

    public float ExtraHappiness()
    {
        var less = MaxPopulation - CurrentPopulation;
        var tenth = MaxPopulation / 100;

        var LHappiness = Happiness += (less / tenth);
        LHappiness = Happiness += 5f;
        return LHappiness;
    }

    public float GetLesserHappiness()
    {
        var more = CurrentPopulation - MaxPopulation;
        var tenth = MaxPopulation / 100;

        var LHappiness = Happiness - (more / tenth);
        return LHappiness;
    }


    public float GetFoodGen()
    {
        return BaseFoodGen + (FoodGen * Farms) - (CurrentPopulation / 1000);
    }
    private void UpdateFood()
    {
        Food += GetFoodGen();
    }

    private void UpdateResource()
    {
        Resources += ResourcesGen * Mines;
    }

    // Use this for initialization
    void Start()
    {
        PartsDone = 0;
        if (Colonized)
        {
            UpdateUi();
        }
        else
        {
            TextContainer.active = false;
            Smiley.enabled = false;
        }

        Jobs = new HashSet<Job>();
        Ships = new HashSet<Ship>();
        Research = new HashSet<Job>();
    }

    private void UpdateUi()
    {
        PopLab.text = CurrentPopulation.ToString();
        CapLab.text = MaxPopulation.ToString();

        if (Happiness > 66)
        {
            Smiley.sprite = Happy;
        }
        else if (Happiness > 33)
        {
            Smiley.sprite = Meh;
        }
        else
        {
            Smiley.sprite = Sad;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (parent != null)
        {
            _angle += RotateSpeed * Time.deltaTime;
            var offset = new Vector3(Mathf.Sin(_angle), Mathf.Cos(_angle), 0) * Radius;
            transform.position = parent.transform.position + offset;
        }


        var y = 0;
        foreach (var ShipSlot in ShipSlots)
        {
            ShipSlots[y].SetActive(false);
            y++;
        }

        var x = 0;
        foreach (var ship in Ships)
        {
            ShipSlots[x].SetActive(true);
            ShipSlots[x].GetComponent<SchipSLot>().Ship = ship;
            x++;
        }
    }

    private void UpdateJobs()
    {
        if (Jobs.Count <= 0 || Jobs.First() == null) return;
        Jobs.First().DoTurn();
        if (Jobs.First().finished())
        {
            Jobs.First().DoJobAction(this);
            Jobs.Remove(Jobs.First());

            if (indicator != null)
            {
                indicator.enabled = true;
            }
        }
    }


    private void UpdateResearch()
    {
        if (Research.Count <= 0 || Research.First() == null) return;
        Research.First().DoTurn();
        if (Research.First().finished())
        {
            Research.First().DoJobAction(this);
            Research.Remove(Research.First());

            if (indicator != null)
            {
                indicator.enabled = true;
            }
        }
    }

    public void AddJob(Job job)
    {
        if (Jobs.Count < 4)
        {
            Jobs.Add(job);
        }
    }

    public void AddResearch(Job job)
    {
        if (Research.Count < 4)
        {
            Research.Add(job);
        }
    }

    public void AddShip(string ship)
    {
        if (Ships.Count < 3)
        {
            Ships.Add(new Ship(this));
        }
    }

    public void RemoveShip(Ship sship)
    {
        Ships.Remove(sship);
    }


    public void Colonize()
    {
        Colonized = true;
        TextContainer.active = true;
        Smiley.enabled = true;
        Food += 100;
        Resources += 200;

        if (indicator != null)
        {
            indicator.enabled = true;
        }
    }
}