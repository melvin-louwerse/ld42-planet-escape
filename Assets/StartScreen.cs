﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void StartScene()
	{
		SceneManager.LoadScene("Universe");
	}

	public void StartBriefing()
	{
		SceneManager.LoadScene("MIssion");
	}

	public void StartBreaking()
	{
		SceneManager.LoadScene("Breaking");
	}


	public void QuitGame()
	{
		Application.Quit();
	}
}
