﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlanetSwitcher : MonoBehaviour
{
    public GameObject Planet;

    public GameObject Galaxy;

    // Use this for initialization
    void Start()
    {
        Button btn = this.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
    }

    private void TaskOnClick()

    {
        Galaxy.GetComponent<Universe>().ActivePlanet = Planet;
    }

    // Update is called once per frame
    void Update()
    {
    }
}