using System;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class MouseHandler : MonoBehaviour
{
    private bool _isDragging = false;
    private Vector3 _lastMousePos;

    public GameObject PlanetUi;

    private double _catchTime = 0.3f;
    private double _lastClickTime;
    public PLanetScreenManager PlanetUiManager;

    public GameObject SelectedPlaceholder;
    private GameObject shipslot;
    private GameObject myLine;
    public GameObject SelectedHolder;

    public LayerMask layer;
    public GameObject EnterButton;

    public Universe Universe;

    // Update is called once per frame
    void Update()
    {
        if (shipslot != null)
        {
            var mousepos = new Vector3(
                Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                Camera.main.ScreenToWorldPoint(Input.mousePosition).y,
                0
            );


            moveLine(shipslot.transform.position, mousepos);
        }

        if (Input.GetMouseButtonDown(2))
        {
            _isDragging = true;
        }

        if (Input.GetMouseButtonUp(2))
        {
            _isDragging = false;
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0) // forward
        {
            Camera.main.orthographicSize += 8;
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0) // back
        {
            if (Camera.main.orthographicSize > 1)
            {
                Camera.main.orthographicSize -= 8;
            }
        }


        if (_isDragging)
        {
            Vector3 currentPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 diff = _lastMousePos - currentPos;
            diff.z = 0;

            Camera.main.transform.Translate(diff, Space.World);
            Universe.ActivePlanet = null;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            var ship = clickedOnShip();
            if (ship != null)
            {
                SelectedPlaceholder.GetComponentInChildren<Text>().text = ship.Name;
                DrawLine(shipslot.transform.position, shipslot.transform.position, Color.red, 0.01f);
            }

            Planet clickedPlanet = clickedOnPlanet();
            if (clickedPlanet != null)
            {
                if (clickedPlanet.Colonized)
                {
                    EnterButton.SetActive(true);
                }
                else
                {
                    EnterButton.SetActive(false);
                }

                SelectedHolder.GetComponent<Image>().sprite = clickedPlanet.GetComponent<SpriteRenderer>().sprite;
                PlanetUiManager.SetPlanet(clickedPlanet);
                Universe.CurrentPlanet = clickedPlanet;
                SelectedPlaceholder.GetComponentInChildren<Text>().text = clickedPlanet.Name;
            }


            if (shipslot != null)
            {
                Planet movetiPlanet = clickedOnPlanet();
                if (movetiPlanet != null)
                {
                    EnterButton.SetActive(false);
                    var sship = shipslot.GetComponent<SchipSLot>().Ship;
                    sship.SetRoute(sship.Current, movetiPlanet);
                    sship.Current.RemoveShip(sship);
                    gameObject.GetComponentInParent<Universe>().AddShip(sship);
                    shipslot = null;
                    destoryLine();
                }
            }

            _lastClickTime = Time.time;
        }

        _lastMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    public void EnterPlanet()
    {
        PlanetUi.SetActive(true);
    }

    private Ship clickedOnShip()
    {
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(
            Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y
        ), Vector2.zero, layer);


        if (hit.collider != null)
        {
            GameObject tempObject = hit.collider.gameObject;

            if (tempObject.CompareTag("Ship"))
            {
                shipslot = tempObject;
                return tempObject.GetComponent<SchipSLot>().Ship;
            }
        }

        return null;
    }

    private Planet clickedOnPlanet()
    {
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(
            Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
            Camera.main.ScreenToWorldPoint(Input.mousePosition).y
        ), Vector2.zero);


        if (hit.collider != null)
        {
            GameObject tempObject = hit.collider.gameObject;
            if (tempObject.CompareTag("Planet"))
            {
                return tempObject.GetComponent<Planet>();
            }
        }

        return null;
    }


    void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f)
    {
        myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.SetColors(color, color);
    }

    void moveLine(Vector3 start, Vector3 end)
    {
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.SetWidth(0.3f, 03f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
    }

    void destoryLine()
    {
        Destroy(myLine);
        myLine = null;
    }
}