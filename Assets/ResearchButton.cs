﻿using UnityEngine;
using UnityEngine.UI;

public class ResearchButton : MonoBehaviour
{
    private Universe _universe;
    public float NeededResourced;
    public float NeededFood;


    // Use this for initialization
    void Start()
    {
        _universe = GameObject.Find("Universe").GetComponent<Universe>();
    }

    // Update is called once per frame
    void Update()
    {
        var Btn = GetComponent<Button>();
        if (Btn == null)
        {
            return;
        }

        if (_universe.CurrentPlanet.Resources < NeededResourced)
        {
            GetComponent<Button>().interactable = false;
        }
        else if (_universe.CurrentPlanet.Food < NeededFood)
        {
            GetComponent<Button>().interactable = false;
        }
        else
        {
            GetComponent<Button>().interactable = true;
        }
    }
}