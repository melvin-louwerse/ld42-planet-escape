using UnityEngine;

public abstract class Job
{
    public int TurnsLeft { get; private set; }
    public Sprite JobSprite { get; private set; }

    public Job(int turnsLeft, Sprite jobSprite)
    {
        this.TurnsLeft = turnsLeft;
        JobSprite = jobSprite;
    }

    public void DoTurn()
    {
        TurnsLeft--;
    }

    public bool finished()
    {
        return TurnsLeft == 0;
    }

    public abstract void DoJobAction(Planet planet);
}